/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  daisyui: {
    themes: [
      {
        dark: {
          "primary": "#52B888",
          "secondary": "#F74976",
          "accent": "#F59526",
          "neutral": "#232531",
          "base-100": "#1A1B24",
          "info": "#00c4ff",
          "success": "#00d6aa",
          "warning": "#ffc300",
          "error": "#e22700",

          "--rounded-box": "1rem", // border radius rounded-box utility class, used in card and other large boxes
          "--rounded-btn": "0.5rem", // border radius rounded-btn utility class, used in buttons and similar element
          "--rounded-badge": "1.9rem", // border radius rounded-badge utility class, used in badges and similar
          "--animation-btn": "0.25s", // duration of animation when you click on button
          "--animation-input": "0.2s", // duration of animation for inputs like checkbox, toggle, radio, etc
          "--btn-focus-scale": "0.95", // scale transform of button when you focus on it
          "--border-btn": "1px", // border width of buttons
          "--tab-border": "1px", // border width of tabs
          "--tab-radius": "0.5rem", // border radius of tabs
        },
        light: {
          "primary": "#52B888",
          "secondary": "#F74976",
          "accent": "#F59526",
          "neutral": "#F8F8F8",
          "base-100": "#E2E2E2",
          "info": "#00c4ff",
          "success": "#00d6aa",
          "warning": "#ffc300",
          "error": "#e22700",
          "control": "#111",


          "--rounded-box": "1rem", // border radius rounded-box utility class, used in card and other large boxes
          "--rounded-btn": "0.5rem", // border radius rounded-btn utility class, used in buttons and similar element
          "--rounded-badge": "1.9rem", // border radius rounded-badge utility class, used in badges and similar
          "--animation-btn": "0.25s", // duration of animation when you click on button
          "--animation-input": "0.2s", // duration of animation for inputs like checkbox, toggle, radio, etc
          "--btn-focus-scale": "0.95", // scale transform of button when you focus on it
          "--border-btn": "1px", // border width of buttons
          "--tab-border": "1px", // border width of tabs
          "--tab-radius": "0.5rem", // border radius of tabs

        }
      }
    ],
  },
  plugins: [require("daisyui"), "prettier-plugin-tailwindcss"]
};
