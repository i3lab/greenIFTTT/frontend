import { createApp } from 'vue'
import App from './App.vue'
import router from '@/router'
import axios from 'axios'
import store from '@/store'
import ToastPlugin from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';


// set default base url
axios.defaults.baseURL = 'http://localhost:8080/api/v3'
const WEBSOCKET_BASEURL = 'ws://localhost:8081/'

// set interceptor for request (add token to header)
axios.interceptors.request.use((config) => {
    const token = store.getUserToken()
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
})

// set interceptor for response (handle error, if any)
axios.interceptors.response.use(
    (response) => response,
    (error) => {
        if (error.response.status === 401 || (typeof error.response.data.error != 'undefined' && error.response.data.error.includes("Token"))) {
            store.resetUserToken()
            router.push('/')
        }
        return Promise.reject(error)
    },
)

const app = createApp(App);
// set axios to vue instance
app.config.globalProperties.$axios = axios
app.config.globalProperties.WS_URL = WEBSOCKET_BASEURL;
app.use(router)
app.use(ToastPlugin, {
    duration: 5000
})
app.mount('#app')
