import { createRouter, createWebHistory } from 'vue-router'
import store from '@/store'

const routes = [
    {
        path: "/",
        name: "Home",
        component: () => import('@/views/Home.vue')
    },
    {
        path: "/login",
        name: "Login",
        component: () => import('@/views/Login.vue')
    },
    { 
        path: "/register", 
        name: "Register", 
        component: () => import('@/views/Register.vue')
    },
    { 
        path: "/devices", 
        name: "Devices", 
        component: () => import('@/views/Devices.vue'), 
        meta: { requiresAuth: true }
    },
    { 
        path: "/routines", 
        name: "Routines", 
        component: () => import('@/views/Routines.vue'), 
        meta: { requiresAuth: true }
    },
    { 
        path: "/routines/:routineId/activities", 
        name: "Activities", 
        component: () => import('@/views/Activities.vue'), 
        meta: { requiresAuth: true }, 
        props: true
    },
    { 
        path: "/activities/:activityId/conditions", 
        name: "Conditions", 
        component: () => import('@/views/Conditions.vue'), 
        meta: { requiresAuth: true }, 
        props: true
    },
    { 
        path: "/chat", 
        name: "Chat", 
        component: () => import('@/views/Chat.vue'), 
        meta: { requiresAuth: true }
    },
    {
        path: "/advices",
        name: "Advices",
        component: () => import('@/views/Advices.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: "/charts",
        name: "Charts",
        component: () => import('@/views/Charts.vue'),
        meta: { requiresAuth: true }
    },
    { 
        path: "/:pathMatch(.*)*", 
        name: "NotFound", 
        component: () => import('@/views/NotFound.vue')
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes,
    scrollBehavior(to, from, savedPosition) {
        return savedPosition || new Promise((resolve) => {
            setTimeout(() => { resolve({ top: 0, behavior: 'smooth' }) }, 300)
        })
    },
    linkActiveClass: 'active'
})

router.beforeEach(async (to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth) && !store.getUserToken()) {
        next({ name: 'Login', query: { redirect: to.fullPath }, replace: true})
    } else {
        next()
    }
})

export default router