export default {
    userToken: null,
    darkMode: null,
    defaultDarkMode: "light", // TODO: change the default mode here...
    setUserToken(userToken) {
        this.userToken = userToken
        localStorage.setItem('auiJWTToken', userToken)
    },
    getUserToken() {
        this.userToken = localStorage.getItem('auiJWTToken') || null
        if (this.userToken) {
            return this.userToken
        }
        return null
    },
    resetUserToken() {
        localStorage.removeItem('auiJWTToken')
        this.userToken = null
    },
    toggleDarkMode() {
        this.darkMode = localStorage.getItem('auiDarkMode') || null
        if (this.darkMode) {
            if(this.darkMode === "dark") {
                this.darkMode = "light"
            } else {
                this.darkMode = "dark"
            }
        } else {
            this.darkMode = this.defaultDarkMode
        }
        localStorage.setItem('auiDarkMode', this.darkMode)
        document.documentElement.setAttribute('data-theme', this.darkMode);
        return this.darkMode
    },
    getDarkMode() {
        this.darkMode = localStorage.getItem('auiDarkMode') || null
        if (!this.darkMode) {
            localStorage.setItem('auiDarkMode', this.defaultDarkMode)
            this.darkMode = this.defaultDarkMode
        }
        return this.darkMode
    }
}